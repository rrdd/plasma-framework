{
    "KPlugin": {
        "Authors": [
            {
                "Email": "kde-artists@kde.org",
                "Name": "The Oxygen Project",
                "Name[ca@valencia]": "The Oxygen Project",
                "Name[ca]": "The Oxygen Project",
                "Name[es]": "El proyecto Oxígeno",
                "Name[eu]": "«Oxygen» proiektua",
                "Name[fr]": "Le projet « Oxygen »",
                "Name[nl]": "Het project Oxygen",
                "Name[sl]": "Projekt kisik",
                "Name[tr]": "Oksijen Projesi",
                "Name[uk]": "Проєкт Oxygen",
                "Name[x-test]": "xxThe Oxygen Projectxx"
            }
        ],
        "Category": "",
        "Description": "Theme done in the Oxygen style",
        "Description[ca@valencia]": "Tema fet en l'estil Oxygen",
        "Description[ca]": "Tema fet en l'estil Oxygen",
        "Description[es]": "Tema realizado al estilo de Oxígeno",
        "Description[eu]": "Oxygen-en estilora egindako gaia",
        "Description[fr]": "Thème réalisé selon un thème « Oxygen »",
        "Description[nl]": "Thema in Oxygen-stijl",
        "Description[sl]": "Teme izdelana v slogu Kisika",
        "Description[tr]": "Oksijen tarzında yapılmış tema",
        "Description[uk]": "Тема в стилі Oxygen",
        "Description[x-test]": "xxTheme done in the Oxygen stylexx",
        "EnabledByDefault": true,
        "Id": "oxygen",
        "License": "GPL",
        "Name": "Oxygen",
        "Name[ca@valencia]": "Oxygen",
        "Name[ca]": "Oxygen",
        "Name[es]": "Oxígeno",
        "Name[eu]": "Oxigenoa",
        "Name[fr]": "Oxygen",
        "Name[nl]": "Oxygen",
        "Name[sl]": "Kisik",
        "Name[tr]": "Oksijen",
        "Name[uk]": "Oxygen",
        "Name[x-test]": "xxOxygenxx",
        "Version": "@KF_VERSION@",
        "Website": "https://plasma.kde.org"
    },
    "X-Plasma-API": "5.0"
}
