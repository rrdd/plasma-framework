{
    "KPlugin": {
        "Authors": [
            {
                "Email": "kde-artists@kde.org",
                "Name": "KDE Visual Design Group",
                "Name[ca@valencia]": "Grup de disseny visual de KDE",
                "Name[ca]": "Grup de disseny visual de KDE",
                "Name[es]": "El grupo de diseño visual de KDE",
                "Name[eu]": "KDE Diseinu bisualeko taldea",
                "Name[fr]": "Groupe de conception graphique de KDE « VDG » (Visual Design Group)",
                "Name[nl]": "KDE Visuele ontwerpgroep",
                "Name[sl]": "Skupina vizualnega designa KDE",
                "Name[tr]": "KDE Görsel Tasarım Grubu",
                "Name[uk]": "Група з візуального дизайну KDE",
                "Name[x-test]": "xxKDE Visual Design Groupxx"
            }
        ],
        "Category": "",
        "Description": "Breeze Light by the KDE VDG",
        "Description[ca@valencia]": "Brisa clara, creat pel VDG de KDE",
        "Description[ca]": "Brisa clara, creat pel VDG del KDE",
        "Description[es]": "Brisa claro por KDE VDG",
        "Description[eu]": "Brisa argia, KDE VDGk egina",
        "Description[fr]": "Breeze clair par l'équipe « KDE VDG »",
        "Description[nl]": "Breeze Light door de KDE VDG",
        "Description[sl]": "Svetla sapica od KDE VDG",
        "Description[tr]": "KDE VDG tarafından Esinti Açık",
        "Description[uk]": "Світла Breeze, автори — KDE VDG",
        "Description[x-test]": "xxBreeze Light by the KDE VDGxx",
        "EnabledByDefault": true,
        "Id": "default",
        "License": "LGPL",
        "Name": "Breeze Light",
        "Name[ca@valencia]": "Brisa clara",
        "Name[ca]": "Brisa clara",
        "Name[es]": "Brisa claro",
        "Name[eu]": "Brisa argia",
        "Name[fr]": "Breeze clair",
        "Name[nl]": "Breeze Light",
        "Name[sl]": "Svetla sapica",
        "Name[tr]": "Esinti Açık",
        "Name[uk]": "Світла Breeze",
        "Name[x-test]": "xxBreeze Lightxx",
        "Version": "@KF_VERSION@",
        "Website": "https://plasma.kde.org"
    },
    "X-Plasma-API": "5.0"
}
